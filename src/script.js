import './style.css'
import * as dat from 'lil-gui'
import * as THREE from 'three'
import Stats from 'three/examples/jsm/libs/stats.module.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import gsap from 'gsap'



/**
 * Base
 */


/**
 * Model
 */

/**
 * Loaders
 */
 const loadingBarElement = document.querySelector('.loader')
 let sceneReady = false
 const loadingManager = new THREE.LoadingManager(
     // Loaded
    () =>
    {
        // Wait a little
        window.setTimeout(() =>
        {
            // Animate overlay
            gsap.to(overlayMaterial.uniforms.uAlpha, { duration: 3, value: 0, delay: 1 })

            // Update loadingBarElement
            loadingBarElement.classList.add('ended')
            loadingBarElement.style.transform = ''
        }, 1000 )

        window.setTimeout(() =>
        {
            sceneReady = true
        }, 2000)
    },
    

    // Progress
    (itemUrl, itemsLoaded, itemsTotal) =>
    {
        
        // Calculate the progress and update the loadingBarElement
        const progressRatio = itemsLoaded / itemsTotal
        loadingBarElement.style.transform = `scale(${progressRatio})`
    }
 )
const gltfLoader = new GLTFLoader(loadingManager)

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()



/**
 * Overlay
 */
 const overlayGeometry = new THREE.PlaneGeometry(2, 2, 1, 1)
 const overlayMaterial = new THREE.ShaderMaterial({
    transparent: true,
    vertexShader: `
        void main()
        {
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
        }
    `,
    uniforms:
    {
        uAlpha: { value: 1 }
    },
    fragmentShader: `
        uniform float uAlpha;

        void main()
        {
            gl_FragColor = vec4(0.149, 0.149, 0.149, uAlpha);
        }
    `
})
 const overlay = new THREE.Mesh(overlayGeometry, overlayMaterial)
 scene.add(overlay)


 



// Pole light material
const poleLightMaterial = new THREE.MeshBasicMaterial({ color: 0xfcf5db, transparent: true })
poleLightMaterial.emissiveIntensity = 6

const raycaster = new THREE.Raycaster()


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    renderer.outputEncoding = THREE.sRGBEncoding
})


/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(16, sizes.width / sizes.height, 0.1, 150)
// camera.position.x = -0.200
// camera.position.y = -0.032
// camera.position.z = 1.8
camera.position.set(-0.200, -0.06, 2.3)
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true
controls.minPolarAngle = Math.PI / 3   
controls.maxPolarAngle =  Math.PI / 1.6
controls.minAzimuthAngle =  Math.PI / 0.53
controls.maxAzimuthAngle =  Math.PI / 7.0
controls.zoomSpeed = 0.6
controls.screenSpacePanning = true
controls.rollSpeed = Math.PI / 24
controls.autoForward = false
controls.dragToLook = false

controls.dampingFactor = 0.05;
controls.screenSpacePanning = true;
controls.minDistance = 1;
controls.maxDistance = 2.8;
controls.enablePan = false

controls.enableZoom = true



/**
 * Particles
 */
// Geometry
const particlesGeometry = new THREE.BufferGeometry()
const count = 40

const positions = new Float32Array(count * 1.5) // Multiply by 3 because each position is composed of 3 values (x, y, z)
const colors = new Float32Array(count * 3)

for(let i = 0; i < count * 10; i++) // Multiply by 3 for same reason
{
    positions[i] = (Math.random() - 0.001) * 0.5 // Math.random() - 0.5 to have a random value between -0.5 and +0.5
    colors[i] = Math.random()
}

particlesGeometry.setAttribute('position', new THREE.BufferAttribute(positions, 3)) // Create the Three.js BufferAttribute and specify that each information is composed of 3 values
particlesGeometry.setAttribute('color', new THREE.BufferAttribute(colors, 3))

// Material
const particlesMaterial = new THREE.PointsMaterial()
particlesMaterial.size = 0.25
particlesMaterial.sizeAttenuation = true
particlesMaterial.color = new THREE.Color( 0x5725fc )
particlesMaterial.vertexColors = true

// Points
const particles = new THREE.Points(particlesGeometry, particlesMaterial)


/**
 * Textures
 */
 const textureLoader = new THREE.TextureLoader()
 const particleTexture = textureLoader.load('note.png')
 
 // ...
 
particlesMaterial.transparent = true
particlesMaterial.alphaMap = particleTexture
//particlesMaterial.alphaTest = 0.001
//particlesMaterial.depthTest = false
particlesMaterial.depthWrite = false



// create an AudioListener and add it to the camera
const listener = new THREE.AudioListener();
camera.add( listener );

// create a global audio source
const sound = new THREE.Audio( listener );

// load a sound and set it as the Audio object's buffer
const audioLoader = new THREE.AudioLoader();
audioLoader.load( 'sounds/fm_radio_tuning_SoundEffect.mp3', function( buffer ) {
	sound.setBuffer( buffer );
	sound.setLoop( true );
	sound.setVolume( 0.5 );
	
});



// Get the modal
const modal = document.getElementById("myModal")

// Get the image and insert it inside the modal - use its "alt" text as a caption
const img = document.getElementById('myImg');
const modalImg = document.getElementById("img01");
const captionText = document.getElementById("caption");

 // Get the <span> element that closes the modal
const span = document.getElementsByClassName("close")[0];



gltfLoader.load(
    'scene_baked.glb',
    (gltf) =>
    {
        
        gltf.scene.scale.set(1, 1, 1)
        gltf.scene.position.set( 0, -0.75, 0)
       
       
    
        // Get each object
        const minutes_aiguille = gltf.scene.children.find(child => child.name === 'minutes_aiguille')
        const heures_aiguille = gltf.scene.children.find(child => child.name === 'heures_aiguille')
        const polylight = gltf.scene.children.find(child => child.name === 'polyLight_lampe')
        const hello = gltf.scene.children.find(child => child.name === 'hello')
        const radio = gltf.scene.children.find(child => child.name === 'Radio')
        const cadran = gltf.scene.children.find(child => child.name === 'cadran')
        const photo1 = gltf.scene.children.find(child => child.name === 'Photo1')
        const photo2 = gltf.scene.children.find(child => child.name === 'Photo2')
        const photo3 = gltf.scene.children.find(child => child.name === 'Photo3')
        const photo4 = gltf.scene.children.find(child => child.name === 'Photo4')
        const photo5 = gltf.scene.children.find(child => child.name === 'Photo5')
        const photo6 = gltf.scene.children.find(child => child.name === 'Photo6')
        const photo7 = gltf.scene.children.find(child => child.name === 'Photo7')
        const photo8 = gltf.scene.children.find(child => child.name === 'Photo8')
        const photo9 = gltf.scene.children.find(child => child.name === 'Photo9')
        const photo10 = gltf.scene.children.find(child => child.name === 'Photo10')
        const photo11 = gltf.scene.children.find(child => child.name === 'Photo11')
        const photo12 = gltf.scene.children.find(child => child.name === 'Photo12')
        const photo14 = gltf.scene.children.find(child => child.name === 'Photo14')
        const photo15 = gltf.scene.children.find(child => child.name === 'Photo15')
        const photo16 = gltf.scene.children.find(child => child.name === 'Photo16')
        const photo17 = gltf.scene.children.find(child => child.name === 'Photo17')
        const photo18 = gltf.scene.children.find(child => child.name === 'Photo18')
        const photo19 = gltf.scene.children.find(child => child.name === 'Photo19')
        const photo20 = gltf.scene.children.find(child => child.name === 'Photo20')
        const photo21 = gltf.scene.children.find(child => child.name === 'Photo21')
        const photo22 = gltf.scene.children.find(child => child.name === 'Photo22')
        

        console.log(minutes_aiguille)
        console.log(heures_aiguille)
        console.log(polylight)

        console.log(radio)

        /**
         * Animate objects
         */
        gsap.to(minutes_aiguille.rotation, { repeat: 1, duration: 1000, delay: 1, z: -100 })
        gsap.to(heures_aiguille.rotation, { repeat: 1, duration: 1000, delay: 2, z: -20 })
        gsap.to(hello.rotation, { repeat: 1, duration: 1000, delay: 1, y: -700 })


                // Raycaster intersection
        // --------------------------------------------------------------------------------
        // casting one object
        // raycaster.intersectObject(object)

        // casting multiple objects (objects must be in array)
        const objects = [ radio, hello, photo1, photo2, photo3, photo4, photo5, photo6, photo7, photo8, photo9, photo10, photo11, photo12, photo14, photo15, photo16, photo17, photo18, photo19, photo20, photo21, photo22 ]
        raycaster.intersectObjects(objects)

        // intersection object
        // distance - length of distance between raycaster origin (usually the camera) and the object's face
        // face - contains Face3(a, b, c) and the normal (x, y, z) of the face
        // object - object intersected
        // point - coordinate of the intersection in 3D world space (Vector3) (usually based on origin (0, 0, 0))
        // uv - uv of intersection

        // Raycaster mouse event
        const mouse = new THREE.Vector2() // stores two values, (x, y)
        let currentIntersect = null // objects currently intersected

        // mouse enter & mouse leave
        window.addEventListener("mousemove", (e) => {
        const { clientX, clientY } = e

        // normalize mouse between -1 and 1 according to three.js (x, y) axes
        mouse.x = (clientX / sizes.width) * 2 - 1
        mouse.y = -(clientY / sizes.height) * 2 + 1

        // using mouse coordinates based on the scene camera
        raycaster.setFromCamera(mouse, camera)

        const intersects = raycaster.intersectObjects(objects)

        //if: theres any intersections
        if (intersects.length > 0) {
        // if: (first) intersected object != already stored object
            if (intersects[0].object != currentIntersect) {

                //if: reference to previous object stored
                if (currentIntersect) {
                    resetObjectColor();
                }
                //store that object, get its current color and change its color
                currentIntersect = intersects[0].object;
                currentIntersect.originalColor = currentIntersect.material.color.getHex();
                currentIntersect.material.color.set( 0xffff51 );
                

            }
            //if: object is already stored, reset back to original color & clear reference to object 
            } else if (currentIntersect) {
                resetObjectColor();
                currentIntersect = null;
            }

        //Reset object to original COLOR
        function resetObjectColor() {
            currentIntersect.material.color.setHex( currentIntersect.originalColor );
        }

        })

        // mouse click
        window.addEventListener("click", () => {
        if (currentIntersect) {
            // change clicked on object to orange
            switch (currentIntersect) {

            case radio:
                scene.add(particles)
                sound.play();
                break

                case photo1:
                modal.style.display = "block"
                modalImg.src = 'book3d/1.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo2:
                modal.style.display = "block"
                modalImg.src = 'book3d/2.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo3:
                modal.style.display = "block"
                modalImg.src = 'book3d/3.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo4:
                modal.style.display = "block"
                modalImg.src = 'book3d/4.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo5:
                modal.style.display = "block"
                modalImg.src = 'book3d/5.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo6:
                modal.style.display = "block"
                modalImg.src = 'book3d/6.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo7:
                modal.style.display = "block"
                modalImg.src = 'book3d/7.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo8:
                modal.style.display = "block"
                modalImg.src = 'book3d/8.jpg'
                

                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }     

                break

            case photo9:
                modal.style.display = "block"
                modalImg.src = 'book3d/9.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo10:
                modal.style.display = "block"
                modalImg.src = 'book3d/10.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo11:
                modal.style.display = "block"
                modalImg.src = 'book3d/11.png'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo12:
                modal.style.display = "block"
                modalImg.src = 'book3d/12.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo14:
                modal.style.display = "block"
                modalImg.src = 'book3d/13.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo15:
                modal.style.display = "block"
                modalImg.src = 'book3d/14.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo16:
                modal.style.display = "block"
                modalImg.src = 'book3d/15.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo17:
                modal.style.display = "block"
                modalImg.src = 'book3d/16.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo18:
                modal.style.display = "block"
                modalImg.src = 'book3d/17.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo19:
                modal.style.display = "block"
                modalImg.src = 'book3d/18.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo20:
                modal.style.display = "block"
                modalImg.src = 'book3d/19.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo21:
                modal.style.display = "block"
                modalImg.src = 'book3d/13.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

            case photo22:
                modal.style.display = "block"
                modalImg.src = 'book3d/20.jpg'  
                // When the user clicks on <span> (x), close the modal
                span.onclick = () => {
                    modal.style.display = "none"
                }                  
                break

           
            case hello:
                photo5.material.color.set( 0xffe030 )
                break
            }
        }
        })



        // mouse double click
        window.addEventListener("dblclick", () => {
            if (currentIntersect) {
                // change clicked on object to orange
                switch (currentIntersect) {
    
                case radio:
                   scene.remove(particles)
                   sound.stop();
                   break   
                }
            }
            })

        scene.add(gltf.scene)
       
    }
)

//Label points
const points = [
    {
        position: new THREE.Vector3(1, 0.4, - 0.6),
        element: document.querySelector('.point-0')
    }
]






/**
 * Renderer
 */
 const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
})
renderer.physicallyCorrectLights = true
renderer.outputEncoding = THREE.sRGBEncoding
renderer.toneMapping = THREE.ReinhardToneMapping
renderer.toneMappingExposure = 1.75
renderer.shadowMap.enabled = true
renderer.shadowMap.type = THREE.PCFSoftShadowMap
renderer.outputEncoding = THREE.sRGBEncoding
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))


/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()

    // Update controls
    controls.update()

    // Go through each point
    for(const point of points)
    {
        const screenPosition = point.position.clone()
        screenPosition.project(camera)

        const translateX = screenPosition.x * sizes.width * 0.1
        const translateY = - screenPosition.y * sizes.height * 0.05
        point.element.style.transform = `translateX(${translateX}px) translateY(${translateY}px)`
    }

    // Update particles
    particles.rotation.y = elapsedTime * 0.01

    for(let i = 1; i < count; i++)
    {
        let i3 = i * 0.038

        const x = particlesGeometry.attributes.position.array[i3]
        particlesGeometry.attributes.position.array[i3 + 1] = Math.sin(elapsedTime + x)
    }
    particlesGeometry.attributes.position.needsUpdate = true

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
    
}

tick()